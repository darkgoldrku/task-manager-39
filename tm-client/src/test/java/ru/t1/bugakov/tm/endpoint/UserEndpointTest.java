package ru.t1.bugakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.bugakov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.bugakov.tm.api.endpoint.IUserEndpoint;
import ru.t1.bugakov.tm.dto.request.user.*;
import ru.t1.bugakov.tm.marker.SoapCategory;
import ru.t1.bugakov.tm.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private List<User> userList;

    @Nullable
    private String token;

    @Before
    public void initEndpoint() throws SQLException {
        userList = new ArrayList<>();
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        @Nullable final User test = userEndpoint.registryUser(new UserRegistryRequest(token, "test" + Math.random(), "random", "test@" + Math.random())).getUser();
        userList.add(test);
        @Nullable final User user = userEndpoint.unlockUser(new UserUnlockRequest(token, "user")).getUser();
        userList.add(user);
    }

    @Test
    @Category(SoapCategory.class)
    public void testLockUnlockUser() throws SQLException {
        @NotNull User test = userList.get(0);
        Assert.assertFalse(test.isLocked());
        test = userEndpoint.lockUser(new UserLockRequest(token, test.getLogin())).getUser();
        Assert.assertTrue(test.isLocked());
        test = userEndpoint.unlockUser(new UserUnlockRequest(token, test.getLogin())).getUser();
        Assert.assertFalse(test.isLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveUser() throws SQLException {
        @NotNull final User test = userList.get(0);
        userEndpoint.removeUser(new UserRemoveRequest(token, test.getLogin()));
        Assert.assertNull(userEndpoint.unlockUser(new UserUnlockRequest(token, test.getLogin())).getUser());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateUserProfile() throws SQLException {
        @NotNull final String newFirstName = "testFirstName";
        @NotNull final String newLastName = "testLastName";
        @NotNull final String newMiddleName = "testMiddleName";
        @Nullable final User updatedUser = userEndpoint.updateUserProfile(new UserUpdateProfileRequest(token, newFirstName, newLastName, newMiddleName)).getUser();
        Assert.assertEquals(newFirstName, updatedUser.getFirstName());
        Assert.assertEquals(newLastName, updatedUser.getLastName());
        Assert.assertEquals(newMiddleName, updatedUser.getMiddleName());
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeUserPassword() throws SQLException {
        @NotNull final String newPassword = "newPassword";
        @NotNull final String oldPassword = userList.get(0).getPasswordHash();
        @Nullable final User updatedUser = userEndpoint.changeUserPassword(new UserChangePasswordRequest(token, newPassword)).getUser();
        Assert.assertNotEquals(oldPassword, updatedUser.getPasswordHash());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRegistryUser() throws SQLException {
        userEndpoint.registryUser(new UserRegistryRequest(token, "testRegistry", "testRegistry", "test@Registry"));
        Assert.assertNotNull(userEndpoint.removeUser(new UserRemoveRequest(token, "testRegistry")).getUser());
    }

}
