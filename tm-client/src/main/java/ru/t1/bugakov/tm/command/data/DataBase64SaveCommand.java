package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataBase64SaveRequest;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        getDomainEndpoint().Base64Save(new DataBase64SaveRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-save-base64";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Save data to BASE64 file";
    }

}
