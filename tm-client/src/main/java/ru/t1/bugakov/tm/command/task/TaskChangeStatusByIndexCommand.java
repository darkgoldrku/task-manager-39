package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        getTaskEndpoint().changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest(getToken(), index, status));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change task status by index.";
    }

}
