package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[USER PROFILE UPDATE]");
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        getUserEndpoint().updateUserProfile(new UserUpdateProfileRequest(getToken(), firstName, lastName, middleName));
    }

    @NotNull
    @Override
    public String getName() {
        return "user-update-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update profile of current user.";
    }

}