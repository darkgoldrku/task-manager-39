package ru.t1.bugakov.tm.api.service;

public interface ITokenService {
    String getToken();

    void setToken(String token);
}
