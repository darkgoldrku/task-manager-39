package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.task.TaskListRequest;
import ru.t1.bugakov.tm.enumerated.SortType;
import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(SortType.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final SortType sort = SortType.toSort(sortType);
        @NotNull final List<Task> tasks = getTaskEndpoint().listTasks(new TaskListRequest(getToken(), sort)).getTasks();
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show list tasks.";
    }

}
