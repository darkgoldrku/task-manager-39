package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.user.UserUnlockRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;

public final class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserEndpoint().unlockUser(new UserUnlockRequest(getToken(), login));
    }

    @NotNull
    @Override
    public String getName() {
        return "user-unlock";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unlock user.";
    }

}
