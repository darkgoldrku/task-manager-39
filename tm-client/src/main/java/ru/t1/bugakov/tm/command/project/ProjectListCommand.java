package ru.t1.bugakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.project.ProjectListRequest;
import ru.t1.bugakov.tm.enumerated.SortType;
import ru.t1.bugakov.tm.model.Project;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(SortType.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final SortType sort = SortType.toSort(sortType);
        @NotNull final List<Project> projects = getProjectEndpoint().listProjects(new ProjectListRequest(getToken(), sort)).getProjects();
        int index = 1;
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show list projects.";
    }

}
