package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDataBaseProperty {

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull String getDatabaseDriver();

    @NotNull String getDatabaseDialect();

    @NotNull String getDatabaseHbm2ddlAuto();

    @NotNull String getDatabaseShowSql();
}
