package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIdRequest(@Nullable String id, @Nullable String name, @Nullable String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public TaskUpdateByIdRequest(@Nullable String token, @Nullable String id, @Nullable String name, @Nullable String description) {
        super(token);
        this.id = id;
        this.name = name;
        this.description = description;
    }

}
