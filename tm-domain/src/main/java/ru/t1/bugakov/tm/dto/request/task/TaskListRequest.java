package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;
import ru.t1.bugakov.tm.enumerated.SortType;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private SortType taskSort;

    public TaskListRequest(@Nullable SortType taskSort) {
        this.taskSort = taskSort;
    }

    public TaskListRequest(@Nullable String token, @Nullable SortType taskSort) {
        super(token);
        this.taskSort = taskSort;
    }

}
