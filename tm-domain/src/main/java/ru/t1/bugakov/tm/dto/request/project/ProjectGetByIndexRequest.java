package ru.t1.bugakov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectGetByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectGetByIndexRequest(@Nullable Integer index) {
        this.index = index;
    }

    public ProjectGetByIndexRequest(@Nullable String token, @Nullable Integer index) {
        super(token);
        this.index = index;
    }

}
