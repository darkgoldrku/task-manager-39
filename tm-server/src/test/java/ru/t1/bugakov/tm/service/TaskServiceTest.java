package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.ITaskService;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.exception.field.DescriptionEmptyException;
import ru.t1.bugakov.tm.exception.field.NameEmptyException;
import ru.t1.bugakov.tm.exception.field.UserIdEmptyException;
import ru.t1.bugakov.tm.model.Task;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private ITaskService taskService;

    @NotNull
    private List<Task> taskList;

    @Before
    public void initService() throws SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService, sqlSessionFactory);
        taskList = new ArrayList<>();
        taskService = new TaskService(connectionService);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("TestTask" + i);
            task.setDescription("TestDescription" + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId("TestParentProject1");
            } else {
                task.setUserId(USER_ID_2);
                task.setProjectId("TestParentProject2");
            }
            taskService.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testCreate() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        taskService.create(USER_ID_1, "TestTaskAdd", "TestDescriptionAdd");
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, taskService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateEmptyUserId() throws SQLException {
        taskService.create("", "TestTaskAdd", "TestDescriptionAdd");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateEmptyName() throws SQLException {
        taskService.create(USER_ID_1, "", "TestDescriptionAdd");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateEmptyDescription() throws SQLException {
        taskService.create(USER_ID_1, "TestTaskAdd", "");
    }

    @Test
    public void testFindAllByProjectIdPositive() throws SQLException {
        Assert.assertEquals(taskService.findAllByProjectId(USER_ID_1, "TestParentProject1"), taskService.findAll(USER_ID_1));
    }

    @Test
    public void testFindAllByProjectIdNegative() throws SQLException {
        Assert.assertNotEquals(taskService.findAllByProjectId(USER_ID_1, "TestParentProject2"), taskService.findAll(USER_ID_1));
    }

    @Test
    public void testUpdateById() throws SQLException {
        @NotNull final String newName = "TestTaskUpdate";
        @NotNull final String newDescription = "TestDescriptionUpdate";
        @NotNull final Task taskForUpdate = taskList.get(0);
        taskService.updateById(taskForUpdate.getUserId(), taskForUpdate.getId(), newName, newDescription);
        Assert.assertEquals(newName, taskList.get(0).getName());
        Assert.assertEquals(newDescription, taskList.get(0).getDescription());
    }

    @Test
    public void testUpdateByIndex() throws SQLException {
        @NotNull final String newName = "TestTaskUpdate";
        @NotNull final String newDescription = "TestDescriptionUpdate";
        @NotNull final Task taskForUpdate = taskList.get(0);
        taskService.updateByIndex(taskForUpdate.getUserId(), 0, newName, newDescription);
        Assert.assertEquals(newName, taskList.get(0).getName());
        Assert.assertEquals(newDescription, taskList.get(0).getDescription());
    }

    @Test
    public void testChangeProjectStatusById() throws SQLException {
        @NotNull final Status newStatus = Status.COMPLETED;
        @NotNull final Task taskForUpdate = taskList.get(0);
        taskService.changeTaskStatusById(taskForUpdate.getUserId(), taskForUpdate.getId(), newStatus);
        Assert.assertEquals(newStatus, taskList.get(0).getStatus());
    }

    @Test
    public void testChangeProjectStatusByIndex() throws SQLException {
        @NotNull final Status newStatus = Status.COMPLETED;
        @NotNull final Task taskForUpdate = taskList.get(0);
        taskService.changeTaskStatusByIndex(taskForUpdate.getUserId(), 0, newStatus);
        Assert.assertEquals(newStatus, taskList.get(0).getStatus());
    }

}
