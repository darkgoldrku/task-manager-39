package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.bugakov.tm.api.repository.ISessionRepository;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.model.Session;
import ru.t1.bugakov.tm.service.ConnectionService;
import ru.t1.bugakov.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Session> SessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @Before
    public void initRepository() throws SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService, sqlSessionFactory);
        SessionList = new ArrayList<>();
        sessionRepository = new SessionRepository(connectionService.getConnection());
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            sessionRepository.add(session);
            SessionList.add(session);
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
        }
    }

    @Test
    public void testAdd() throws SQLException {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Session session = new Session();
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final Session actualSession = sessionRepository.findByIndex(sessionRepository.getSize() - 1);
        Assert.assertNotNull(actualSession);
    }

    @Test
    public void testAddAll() throws SQLException {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Session> sessions = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            sessions.add(new Session());
        }
        sessionRepository.add(sessions);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClear() throws SQLException {
        final int expectedNumberOfEntries = 0;
        sessionRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() throws SQLException {
        @NotNull final List<Session> sessions = new ArrayList<>();
        sessionRepository.clear(USER_ID_1);
        Assert.assertEquals(sessions, sessionRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(sessions, sessionRepository.findAll(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() throws SQLException {
        sessionRepository.clear("other_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testFindAll() throws SQLException {
        @NotNull final List<Session> sessions = sessionRepository.findAll();
        Assert.assertEquals(SessionList, sessions);
    }

    @Test
    public void testFindAllForUser() throws SQLException {
        @NotNull final List<Session> sessions = sessionRepository.findAll(USER_ID_1);
        Assert.assertEquals(SessionList.subList(0, 5), sessions);
    }

    @Test
    public void testFindByIdPositive() throws SQLException {
        for (@NotNull final Session session : SessionList) {
            Assert.assertNotNull(sessionRepository.findById(session.getId()));
            Assert.assertEquals(session, sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() throws SQLException {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findById(id));
    }

    @Test
    public void testFindByIdForUser() throws SQLException {
        for (@NotNull final Session session : SessionList) {
            if (session.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(sessionRepository.findById(USER_ID_1, session.getId()));
                Assert.assertEquals(session, sessionRepository.findById(session.getId()));
            } else Assert.assertNull(sessionRepository.findById(USER_ID_1));
        }
    }

    @Test
    public void testFindByIndex() throws SQLException {
        @NotNull final Session expected1 = SessionList.get(0);
        @NotNull final Session expected2 = SessionList.get(sessionRepository.getSize() - 1);
        @NotNull final Session expected3 = SessionList.get(sessionRepository.getSize() / 2);
        Assert.assertEquals(expected1, sessionRepository.findByIndex(0));
        Assert.assertEquals(expected2, sessionRepository.findByIndex(sessionRepository.getSize() - 1));
        Assert.assertEquals(expected3, sessionRepository.findByIndex(sessionRepository.getSize() / 2));
        Assert.assertNull(sessionRepository.findByIndex(sessionRepository.getSize()));
    }

    @Test
    public void testFindByIndexForUser() throws SQLException {
        @NotNull final Session expected1 = SessionList.get(0);
        @NotNull final Session expected2 = SessionList.get(5);
        Assert.assertEquals(expected1, sessionRepository.findByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, sessionRepository.findByIndex(USER_ID_2, 0));
        Assert.assertNull(sessionRepository.findByIndex("test user", 0));
    }

    @Test
    public void testGetSize() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionRepository.getSize(USER_ID_1));
        Assert.assertEquals(0, sessionRepository.getSize("test user"));
    }

    @Test
    public void testRemovePositive() throws SQLException {
        for (@NotNull final Session session : SessionList) {
            sessionRepository.remove(session);
            Assert.assertNull(sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void testRemoveNegative() throws SQLException {
        @NotNull final Session session = new Session();
        sessionRepository.remove(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() throws SQLException {
        for (@NotNull final Session session : SessionList) {
            Assert.assertNotNull(sessionRepository.removeById(session.getId()));
            Assert.assertNull(sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() throws SQLException {
        @NotNull final String randomId = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.removeById(randomId));
    }

    @Test
    public void testRemoveByIdForUser() throws SQLException {
        for (@NotNull final Session session : SessionList) {
            if (session.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(sessionRepository.removeById(USER_ID_1, session.getId()));
            } else Assert.assertNull(sessionRepository.removeById(USER_ID_1, session.getId()));
        }
    }

    @Test
    public void testRemoveByIndex() throws SQLException {
        @NotNull final Session expected1 = SessionList.get(0);
        @NotNull final Session expected2 = SessionList.get(sessionRepository.getSize() - 1);
        Assert.assertEquals(expected1, sessionRepository.removeByIndex(0));
        Assert.assertEquals(expected2, sessionRepository.removeByIndex(sessionRepository.getSize() - 1));
        Assert.assertNull(sessionRepository.removeByIndex(sessionRepository.getSize()));
    }

    @Test
    public void testRemoveByIndexForUser() throws SQLException {
        @NotNull final Session expected1 = SessionList.get(0);
        @NotNull final Session expected2 = SessionList.get(5);
        Assert.assertEquals(expected1, sessionRepository.removeByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, sessionRepository.removeByIndex(USER_ID_2, 0));
        Assert.assertNull(sessionRepository.removeByIndex("test user", 0));
    }

}
