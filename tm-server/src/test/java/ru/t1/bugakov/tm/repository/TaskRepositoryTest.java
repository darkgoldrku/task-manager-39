package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.service.ConnectionService;
import ru.t1.bugakov.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @Before
    public void initRepository() throws SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService, sqlSessionFactory);
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository(connectionService.getConnection());
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("TestTask" + i);
            task.setDescription("TestDescription" + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId("TestParentProject1");
            } else {
                task.setUserId(USER_ID_2);
                task.setProjectId("TestParentProject2");
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAdd() throws SQLException {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task task = new Task();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findByIndex(taskRepository.getSize() - 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void testAddAll() throws SQLException {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Task> tasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            tasks.add(new Task());
        }
        taskRepository.add(tasks);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClear() throws SQLException {
        final int expectedNumberOfEntries = 0;
        taskRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() throws SQLException {
        @NotNull final List<Task> tasks = new ArrayList<>();
        taskRepository.clear(USER_ID_1);
        Assert.assertEquals(tasks, taskRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(tasks, taskRepository.findAll(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() throws SQLException {
        taskRepository.clear("other_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testFindAll() throws SQLException {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    public void testFindAllForUser() throws SQLException {
        @NotNull final List<Task> tasks = taskRepository.findAll(USER_ID_1);
        Assert.assertEquals(taskList.subList(0, 5), tasks);
    }

    @Test
    public void testFindByIdPositive() throws SQLException {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.findById(task.getId()));
            Assert.assertEquals(task, taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() throws SQLException {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findById(id));
    }

    @Test
    public void testFindByIdForUser() throws SQLException {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(taskRepository.findById(USER_ID_1, task.getId()));
                Assert.assertEquals(task, taskRepository.findById(task.getId()));
            } else Assert.assertNull(taskRepository.findById(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testFindByIndex() throws SQLException {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(taskRepository.getSize() - 1);
        @NotNull final Task expected3 = taskList.get(taskRepository.getSize() / 2);
        Assert.assertEquals(expected1, taskRepository.findByIndex(0));
        Assert.assertEquals(expected2, taskRepository.findByIndex(taskRepository.getSize() - 1));
        Assert.assertEquals(expected3, taskRepository.findByIndex(taskRepository.getSize() / 2));
        Assert.assertNull(taskRepository.findByIndex(taskRepository.getSize()));
    }

    @Test
    public void testFindByIndexForUser() throws SQLException {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(5);
        Assert.assertEquals(expected1, taskRepository.findByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, taskRepository.findByIndex(USER_ID_2, 0));
        Assert.assertNull(taskRepository.findByIndex("test user", 0));
    }

    @Test
    public void testFindAllByProjectIdPositive() throws SQLException {
        Assert.assertEquals(taskRepository.findAllByProjectId(USER_ID_1, "TestParentProject1"), taskRepository.findAll(USER_ID_1));
    }

    @Test
    public void testFindAllByProjectIdNegative() throws SQLException {
        Assert.assertNotEquals(taskRepository.findAllByProjectId(USER_ID_1, "TestParentProject2"), taskRepository.findAll(USER_ID_1));
    }

    @Test
    public void testGetSize() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(0, taskRepository.getSize("test user"));
    }

    @Test
    public void testRemovePositive() throws SQLException {
        for (@NotNull final Task task : taskList) {
            taskRepository.remove(task);
            Assert.assertNull(taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testRemoveNegative() throws SQLException {
        @NotNull final Task task = new Task();
        taskRepository.remove(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() throws SQLException {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.removeById(task.getId()));
            Assert.assertNull(taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() throws SQLException {
        @NotNull final String randomId = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.removeById(randomId));
    }

    @Test
    public void testRemoveByIdForUser() throws SQLException {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(taskRepository.removeById(USER_ID_1, task.getId()));
            } else Assert.assertNull(taskRepository.removeById(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testRemoveByIndex() throws SQLException {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(taskRepository.getSize() - 1);
        Assert.assertEquals(expected1, taskRepository.removeByIndex(0));
        Assert.assertEquals(expected2, taskRepository.removeByIndex(taskRepository.getSize() - 1));
        Assert.assertNull(taskRepository.removeByIndex(taskRepository.getSize()));
    }

    @Test
    public void testRemoveByIndexForUser() throws SQLException {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(5);
        Assert.assertEquals(expected1, taskRepository.removeByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, taskRepository.removeByIndex(USER_ID_2, 0));
        Assert.assertNull(taskRepository.removeByIndex("test user", 0));
    }

}
