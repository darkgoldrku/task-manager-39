package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IAbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {


    void add(@NotNull final String userId, @NotNull final M model) throws SQLException;

    List<M> findAll(@NotNull final String userId) throws SQLException;

    List<M> findAllWithSort(@NotNull final String userId, @Nullable final String sortField) throws SQLException;

    int getSize(@NotNull final String userId) throws SQLException;

    M findById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    M findByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException;

    boolean existsById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    void clear(@NotNull final String userId) throws SQLException;

    void remove(@Nullable final String userId, @Nullable final M model) throws SQLException;

    void removeById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    void removeByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException;

    void removeAll(@NotNull String userId, @NotNull Collection<M> models) throws SQLException;

}
