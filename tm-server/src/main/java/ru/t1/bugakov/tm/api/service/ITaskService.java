package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskService extends IAbstractUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws SQLException;

    @NotNull
    Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    Task changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws SQLException;

    @NotNull
    Task changeTaskStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) throws SQLException;

}
