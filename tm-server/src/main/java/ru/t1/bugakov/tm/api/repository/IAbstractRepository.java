package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {


    void add(@NotNull final M model) throws SQLException;

    void add(@NotNull Collection<M> models) throws SQLException;

    //@NotNull
    //Collection<M> set(@NotNull Collection<M> models) throws SQLException;

    List<M> findAll() throws SQLException;

    List<M> findAllWithSort(@NotNull final Comparator<M> comparator) throws SQLException;

    int getSize() throws SQLException;

    M findById(@NotNull final String id) throws SQLException;

    M findByIndex(@NotNull final Integer index) throws SQLException;

    boolean existsById(@NotNull final String id) throws SQLException;

    void clear() throws SQLException;

    void remove(@NotNull final M model) throws SQLException;

    void removeById(@NotNull final String id) throws SQLException;

    void removeByIndex(@NotNull final Integer index) throws SQLException;

    void removeAll(@NotNull Collection<M> models) throws SQLException;
}
