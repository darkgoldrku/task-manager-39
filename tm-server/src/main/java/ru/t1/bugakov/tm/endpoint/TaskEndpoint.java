package ru.t1.bugakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.bugakov.tm.api.service.IProjectTaskService;
import ru.t1.bugakov.tm.api.service.IServiceLocator;
import ru.t1.bugakov.tm.api.service.ITaskService;
import ru.t1.bugakov.tm.dto.request.task.*;
import ru.t1.bugakov.tm.dto.response.task.*;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Session;
import ru.t1.bugakov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@WebService(endpointInterface = "ru.t1.bugakov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskGetByIdResponse getTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIdRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Task task = getTaskService().findById(userId, id);
        return new TaskGetByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskGetByIndexResponse getTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIndexRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Task task = getTaskService().findByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListByProjectIdResponse listTasksByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListByProjectIdRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable String sort = null;
        if (request.getTaskSort() != null) sort = request.getTaskSort().name();
        @NotNull final List<Task> tasks = getTaskService().findAllWithSort(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Task task = getTaskService().findById(userId, id);
        getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIndexRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Task task = getTaskService().findByIndex(userId, index);
        getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskToProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
