package ru.t1.bugakov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.api.repository.ISessionRepository;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.ISessionService;
import ru.t1.bugakov.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public @NotNull IAbstractUserOwnedRepository<Session> getRepository(@NotNull final SqlSession session) {
        return session.getMapper(ISessionRepository.class);
    }

}
