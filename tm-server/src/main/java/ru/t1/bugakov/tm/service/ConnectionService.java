package ru.t1.bugakov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.api.repository.ISessionRepository;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IDataBaseProperty;

import javax.sql.DataSource;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IDataBaseProperty dataBaseProperties;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull IDataBaseProperty dataBaseProperties) {
        this.dataBaseProperties = dataBaseProperties;
        this.sqlSessionFactory = getSqlSessionFactory();

    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    @SneakyThrows
    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String username = dataBaseProperties.getDatabaseUsername();
        @NotNull final String password = dataBaseProperties.getDatabasePassword();
        @NotNull final String url = dataBaseProperties.getDatabaseUrl();
        @NotNull final String driver = dataBaseProperties.getDatabaseDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("tm", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
