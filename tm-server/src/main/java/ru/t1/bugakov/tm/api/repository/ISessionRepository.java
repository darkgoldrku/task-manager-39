package ru.t1.bugakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.model.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository extends IAbstractUserOwnedRepository<Session> {

    @Override
    @Insert("INSERT INTO " + DBConstants.TABLE_SESSION + " (id, user_id, created, role) " +
            "VALUES (#{id},#{userId},#{created},#{role})")
    void add(@NotNull Session session) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id")
    })
    List<Session> findAll(final @NotNull @Param("userId") String userId) throws SQLException;

    @Override
    @Select("SELECT COUNT(*) FROM tm_session WHERE user_id = #{userID}")
    int getSize(final @NotNull @Param("userId") String userId) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id")
    })
    Session findById(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "id")
    })
    Session findByIndex(final @NotNull @Param("userId") String userId, final @NotNull @Param("index") Integer index) throws SQLException;

    @Override
    @Delete("DELETE FROM tm_session WHERE user_id = #{user_id}")
    void clear(final @NotNull @Param("user_id") String userId) throws SQLException;

    @Override
    @Delete("DELETE FROM tm_session WHERE user_id = #{user_id} AND id = #{id}")
    void remove(final @Nullable @Param("user_id") String userId, final @Nullable Session model) throws SQLException;

    @Update("UPDATE tm_session SET user_id = #{userId}, created = #{created}, role = #{role} WHERE id = #{id}")
    void update(@NotNull Session session) throws SQLException;

}
