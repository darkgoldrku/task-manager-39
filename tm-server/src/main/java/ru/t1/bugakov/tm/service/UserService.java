package ru.t1.bugakov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.api.service.*;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.exception.entity.UserNotFoundException;
import ru.t1.bugakov.tm.exception.field.*;
import ru.t1.bugakov.tm.exception.user.ExistsEmailException;
import ru.t1.bugakov.tm.exception.user.ExistsLoginException;
import ru.t1.bugakov.tm.model.User;
import ru.t1.bugakov.tm.util.HashUtil;

import java.sql.SQLException;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.projectService = projectService;
        this.taskService = taskService;
        this.propertyService = propertyService;
    }

    @Override
    protected @NotNull IUserRepository getRepository(@NotNull SqlSession session) {
        return session.getMapper(IUserRepository.class);
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = getRepository(session);
            repository.add(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull SqlSession connection = getSqlSession();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull SqlSession connection = getSqlSession();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        try (@NotNull final SqlSession connection = getSqlSession()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findByLogin(login);
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws SQLException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user;
        try (@NotNull final SqlSession connection = getSqlSession()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findByLogin(email);
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public void remove(@Nullable final User model) throws SQLException {
        if (model == null) return;
        super.remove(model);
        @NotNull final String userId = model.getId();
        taskService.clear(userId);
        projectService.clear(userId);
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        remove(user);
    }

    @Override
    public void removeByEmail(@Nullable final String email) throws SQLException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        remove(user);
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable String password) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull SqlSession connection = getSqlSession();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull SqlSession connection = getSqlSession();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) return false;
        try (@NotNull final SqlSession connection = getSqlSession()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.isLoginExist(login);
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws SQLException {
        if (email == null || email.isEmpty()) return false;
        try (@NotNull final SqlSession connection = getSqlSession()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.isEmailExist(email);
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull SqlSession connection = getSqlSession();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.lockUserByLogin(login);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull SqlSession connection = getSqlSession();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.unlockUserByLogin(login);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
