package ru.t1.bugakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.bugakov.tm.api.service.IAuthService;
import ru.t1.bugakov.tm.api.service.IServiceLocator;
import ru.t1.bugakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.bugakov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.bugakov.tm.dto.request.user.UserProfileRequest;
import ru.t1.bugakov.tm.dto.response.result.UserLoginResponse;
import ru.t1.bugakov.tm.dto.response.result.UserLogoutResponse;
import ru.t1.bugakov.tm.dto.response.user.UserProfileResponse;
import ru.t1.bugakov.tm.model.Session;
import ru.t1.bugakov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;

@WebService(endpointInterface = "ru.t1.bugakov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) throws SQLException {
        @NotNull final String token = getAuthService().login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) throws SQLException {
        @Nullable final Session session = check(request);
        getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) throws SQLException {
        @Nullable final Session session = check(request);
        @Nullable final User user = getServiceLocator().getUserService().findById(session.getUserId());
        return new UserProfileResponse(user);
    }

}
